import swaggerUi from 'swagger-ui-express';
import { Router } from 'express';
import * as yaml from 'js-yaml';
import fs from 'fs';
const documentoApi = yaml.safeLoad(fs.readFileSync('src/modules/documents/v0/openapi.yaml', 'utf8'))

const docsRouter = Router();
const options = {
    customJs: '/doc.js',
    customCssUrl: '/doc.css'
}
/*Agregano versiones del documento!!!*/
docsRouter.use('/', swaggerUi.serve);
docsRouter.get('/v0', swaggerUi.setup(documentoApi, options));


export default docsRouter