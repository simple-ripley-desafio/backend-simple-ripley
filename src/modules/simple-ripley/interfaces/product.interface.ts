export interface IRequestProduct {
    uniqueID: string
    partNumber: string
    name: string
    locals:
    {
        outOfStockList:
        {
            xCatEntryQuantity: boolean
            blacklist: boolean
        }
        unavailableList:
        {
            blacklist: boolean
        }
        promotion:
        {
            stock: boolean
            badge: boolean
        }
    }
    fullImage: string
    images: string[]
    prices:
    {
        offerPrice: number
        listPrice: number
        cardPrice: number
        discount: number
        discountPercentage: number
        ripleyPuntos: number
        $$cache:
        {
            cached: boolean
            created: number
        }
        formattedOfferPrice: string
        formattedListPrice: string
        formattedCardPrice: string
        formattedDiscount: string
    }
    shortDescription: string
    longDescription: string
    definingAttributes: definingAttributes[]
    attributes: attributes[]
    shipping:
    {
        rTienda: boolean
        dDomicilio: boolean
        rCercano: boolean
        cashOnDelivery: boolean
    }
    parentProductID: string
    xCatEntryCategory: string
    colors: colors[]
    productString: string
    SKUs: skus[]
    isMarketplaceProduct: boolean
    marketplace:
    {
        shopName: string
        shopId: number
    }
    warranties: warranties[]
    url: string
    thumbnailImage: string
    simple:
    {
        lists:
        {
            outOfStockList: {}
            unavailableList: {}
            promotion: {}
        }
        isUnavailable: boolean
        isOutOfStock: boolean
        isMarketplaceProduct: boolean
    }
    single: boolean
    variations: {}

}

export interface IRequestErrorProduct {
    error: {
        message: string
    }
}

interface definingAttributes {
    displayable: boolean
    id: string
    identifier: string
    name: string
    usage: string
    values: definingAttributesValues[]
}

interface definingAttributesValuesExtendedValue {
    value: string
    key: string
}
interface definingAttributesValues {
    values: string
    extendedValue: definingAttributesValuesExtendedValue[]
    identifier: string
    uniqueID: string
    slug: string
}

interface attributes {
    displayable: boolean
    id: string
    identifier: string
    name: string
    usage: string
    value: string
}

interface colors {
    hex: string
    name: string
    slug: string
    uniqueID: string
}

interface skusAttributesValues {
    values: string
    identifier: string
    uniqueID: string
}

interface skusAttributes {
    usage: string
    Values: skusAttributesValues[]
    searchable: string
    identifier: string
    comparable: string
    name: string
    uniqueID: string
    displayable: string
}

interface SkusPrice {
    SKUPriceDescription: string
    SKUPriceValue: string
    SKUPriceUsage: string
}

interface skus {
    Attributes: skusAttributes[]
    Price: SkusPrice[]
    SKUUniqueID: string
    partNumber: string
    ineligible: boolean
}

interface warrantiesPrice {
    priceValue: number
    priceUsage: string
    formattedPriceValue: string
}

interface warranties {
    shortDescription: string
    Price: warrantiesPrice[]
    type: string
    name: string
    quantity: string
    partNumber: string
    thumbnail: string
    uniqueID: string
}

