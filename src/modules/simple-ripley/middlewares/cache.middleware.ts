import { NextFunction, Request, Response } from 'express';
import RedisConexion from '../../../server/databases/redis';
import { IResponse } from '../../../interfaces/respuestas.interface';


export function cacheByIdMiddleware(req: Request, res: Response, next: NextFunction) {
    try {
        const { productId } = req.params
        RedisConexion.clientRedis.get(productId, (err, data) => {
            if (err) throw err
            if (data !== null) {
                const product: IResponse = JSON.parse(data)
                res.status(product.estado).send(product)
            } else {
                next()
            }
        })

    } catch (err) {
        next()
    }
}

export function cachePartNumberMiddleware(req: Request, res: Response, next: NextFunction) {
    try {
        const { partNumber } = req.params
        RedisConexion.clientRedis.get(partNumber, (err, data) => {
            if (err) throw err
            if (data !== null) {
                const product: IResponse = JSON.parse(data)
                res.status(product.estado).send(product)
            } else {
                next()
            }
        })

    } catch (err) {
        next()
    }
}

export function cachePartNumbersMiddleware(req: Request, res: Response, next: NextFunction) {
    try {
        const { partNumbers, format } = req.query
        const query = (format !== undefined) ? `partNumbers=${partNumbers}&format=${format}` : `partNumbers=${partNumbers}&format=`;
        console.log({ query })
        RedisConexion.clientRedis.get(query, (err, data) => {
            if (err) throw err
            if (data !== null) {
                const products: IResponse = JSON.parse(data)
                console.log({ cacheProducts: products })
                res.status(products.estado).send(products)
            } else {
                next()
            }
        })

    } catch (err) {
        next()
    }
}