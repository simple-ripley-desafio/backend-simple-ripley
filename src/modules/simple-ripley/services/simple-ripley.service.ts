import axios from 'axios';
import { API_RIPLEY, url } from '../../../env_config'
import { IRequestErrorProduct, IRequestProduct } from '../interfaces/product.interface'
import { IResponse } from '../../../interfaces/respuestas.interface';
import RedisConexion from '../../../server/databases/redis';

export const requestProductsById = async (productId: string): Promise<IResponse> => {
    try {
        const response = await axios.get<IRequestProduct | IRequestErrorProduct>(`${API_RIPLEY}${url.PATH_BY_ID}${productId}`)
        const product = {
            estado: response.status,
            descripcion: 'Request ById',
            resultado: response.data
        };
        RedisConexion.clientRedis.setex(productId, 120, JSON.stringify(product));
        return product;
    } catch (error) {
        return {
            estado: error.response.status,
            descripcion: 'Request ById ERROR',
            resultado: error.response.data
        };

    }
}

export const requestProductPartNumber = async (partNumber: string): Promise<IResponse> => {
    try {
        const response = await axios.get<IRequestProduct | IRequestErrorProduct>(`${API_RIPLEY}${url.PATH_PRODUCTS}${partNumber}`)
        const product = {
            estado: response.status,
            descripcion: 'Request ByPartNumber',
            resultado: response.data
        };
        RedisConexion.clientRedis.setex(partNumber, 120, JSON.stringify(product));
        return product;
    } catch (error) {
        return {
            estado: error.response.status,
            descripcion: 'Request ByPartNumber ERROR',
            resultado: error.response.data
        };

    }
}

export const requestProductsPartNumber = async (partNumbers: string, format: string = ''): Promise<IResponse> => {
    try {
        const path = `${API_RIPLEY}${url.PATH_PART_NUMBERS}`
        const query = `partNumbers=${partNumbers}&format=${format}`
        const response = await axios.get<IRequestProduct[] | IRequestErrorProduct>(`${path}${query}`)
        const products = {
            estado: response.status,
            descripcion: 'Request ByPartNumbers',
            resultado: response.data
        };
        RedisConexion.clientRedis.setex(query, 120, JSON.stringify(products));
        return products
    } catch (error) {
        return {
            estado: error.response.status,
            descripcion: 'Request ByPartNumbers ERROR',
            resultado: error.response.data
        };

    }
}