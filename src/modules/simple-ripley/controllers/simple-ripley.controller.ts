import { Request, Response } from 'express';
import { requestProductsById, requestProductPartNumber, requestProductsPartNumber } from '../services/simple-ripley.service'
import { parse } from 'querystring'

export const getById = async (req: Request, res: Response) => {
    const { productId } = req.params
    const data = await requestProductsById(productId)
    res.status(data.estado).send(data)
}

export const getPartNumber = async (req: Request, res: Response) => {
    const { partNumber } = req.params
    const data = await requestProductPartNumber(partNumber)
    res.status(data.estado).send(data)

}
export const getPartNumbers = async (req: Request, res: Response) => {
    const { partNumbers, format } = req.query
    console.log({ partNumbers, format, })
    const data = await requestProductsPartNumber(partNumbers, format)
    res.status(data.estado).send(data)
}

