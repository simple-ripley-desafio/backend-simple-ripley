import { Router } from "express";
import { getById, getPartNumber, getPartNumbers } from '../controllers/simple-ripley.controller';
import { cachePartNumbersMiddleware, cacheByIdMiddleware, cachePartNumberMiddleware } from "../middlewares/cache.middleware";


const simpleRipleyRoutes = Router();
simpleRipleyRoutes.get('/products/by-id/:productId', [cacheByIdMiddleware], getById);
simpleRipleyRoutes.get('/products/by-partNumber/:partNumber', [cachePartNumberMiddleware], getPartNumber);
simpleRipleyRoutes.get('/products/by-partNumbers', [cachePartNumbersMiddleware], getPartNumbers);


export default simpleRipleyRoutes;
