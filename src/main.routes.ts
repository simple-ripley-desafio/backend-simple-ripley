import { Router } from "express";
import docsRouter from "./modules/documents";
import simpleRipleyRoutes from './modules//simple-ripley/routes/simple-ripley.routes'

const mainRouter = Router();

mainRouter.use('/simple-ripley', simpleRipleyRoutes);
mainRouter.use('/api-docs', docsRouter);

export default mainRouter;
