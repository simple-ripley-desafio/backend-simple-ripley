const PORT: number = Number(process.env.PORT) || 3000;

const API_RIPLEY: string = process.env.API_RIPLEY || 'https://simple.ripley.cl/';
const PATH_BY_ID: string = process.env.PATH_BY_ID || 'api/v2/products/by-id/';
const PATH_PRODUCTS: string = process.env.PATH_PRODUCTS || 'api/v2/products/';
const PATH_PART_NUMBERS: string = process.env.PATH_PART_NUMBERS || 'api/v2/products?';
const REDIS_HOST: string = process.env.REDIS_HOST || 'redis-12636.c1.us-central1-2.gce.cloud.redislabs.com';
const REDIS_PORT: number = Number(process.env.REDIS_PORT) || 12636;
const REDIS_KEY: string = process.env.REDIS_KEY || 'b98JlZazIArqEcAq9mFJ7Dx3nSJeOe5S';


const url = {
  PATH_BY_ID,
  PATH_PRODUCTS,
  PATH_PART_NUMBERS
}

export {
  PORT,
  REDIS_HOST,
  REDIS_PORT,
  REDIS_KEY,
  API_RIPLEY,
  url
};

