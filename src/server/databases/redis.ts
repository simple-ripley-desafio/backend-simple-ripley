import { REDIS_HOST, REDIS_PORT, REDIS_KEY } from "../../env_config";
import { RedisClient } from "redis";

export default class RedisConexion {
    static instanciaRedis: boolean = false;
    static clientRedis: RedisClient;

    constructor() {
        RedisConexion.connectar();
    }

    static connectar() {
        if (this.instanciaRedis) {
            return this.clientRedis;
        } else {
            this.clientRedis = new RedisClient({ host: REDIS_HOST, password: REDIS_KEY, port: REDIS_PORT });
            this.clientRedis.on('connect', () => {
                console.log(`conectado a redis. Host: ${REDIS_HOST},  Puerto: ${REDIS_PORT}`)
            });
            this.clientRedis.on('error', (err) => { console.log(`Conexión con REDIS no establecida: ${err}`) });
        }
        return this.clientRedis;
    }
}
