import express from "express";
import RedisConexion from './databases/redis'
/* import firebaseInit from '../services/firebase.service';*/
export default class Servidor {
  app: express.Application;
  puerto: number;
  clientRedis: RedisConexion
  /*   firebase: firebaseInit;
   */
  constructor(port: number) {
    this.app = express();
    this.puerto = port;
    this.clientRedis = new RedisConexion();
    /* this.firebase = new firebaseInit(); */
  }

  static init(port: number = 3000) {
    return new Servidor(port);
  }

  iniciar(callback: any) {
    this.app.listen(this.puerto, callback);
    /* this.firebase.init(); */
  }
}
