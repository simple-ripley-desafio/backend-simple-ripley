import Server from './server/servidor';
import * as bodyParser from 'body-parser';
import cors from 'cors';
import { Request, Response, NextFunction, ErrorRequestHandler } from "express";
import helmet from 'helmet';
import morgan from 'morgan';
import { PORT } from './env_config';
import mainRouter from './main.routes';




const server = Server.init(PORT);

/* opciones de CORS */
const options: cors.CorsOptions = {
  origin: "*",
  credentials: false
};

/* Middlewares */
server.app.use(bodyParser.urlencoded({ extended: false }));
server.app.use(bodyParser.json());
server.app.use(morgan('common'));
server.app.use(cors(options));
server.app.use(helmet());
server.app.use("/", mainRouter);

/* Iniciar Servidor */
server.iniciar(() => {
  console.log(`Servidor Express corriendo en puerto: ${PORT}`);
});

//rutas no definidas
server.app.get("/*", (req: Request, res: Response) => {
  res.status(400).send("ruta no existe");
});

//Capturar errores
server.app.use(
  (
    err: ErrorRequestHandler,
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    if (err) {
      res.status(500).send({ error: err });
      next();
    }
  }
);

export default server;
