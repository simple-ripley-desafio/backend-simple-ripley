# Build1

FROM node:13.2.0-alpine3.10 as builder
WORKDIR /usr/src/app
RUN npm i typescript -g
RUN npm i webpack -g
COPY package* ./
COPY tsconfig.json ./
COPY webpack* ./
COPY src/ src/
RUN npm i
RUN npm run build

# Package
FROM node:13.2.0-alpine3.10 as package
WORKDIR /usr/src/app
COPY --from=builder /usr/src/app/package* ./
COPY --from=builder /usr/src/app/node_modules ./node_modules
COPY --from=builder /usr/src/app/src/documents ./src/documents
COPY --from=builder /usr/src/app/build ./build
CMD ["node", "./build/index.js"]